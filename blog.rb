require 'sinatra'
require 'sinatra/namespace'
require 'json'
require 'sinatra/json'

set :port, 9000

#con esto ponemos ruts relativas en lugar de absolutas.
namespace '/api/v1' do

    before do
        content_type 'application/json'
    end

    # List posts: obtener la lista. Cargamos los post y los publicamos/mostramos en formato JSON
    get '/posts' do
        posts = load_posts
        posts.to_json
    end


    #Nuevo post. Con valid_json intenta parsearlo y si no devulve un 400 y corta la función.
    post '/posts' do
      requestContent = request.body.read
        if !valid_json?(requestContent)
            halt 400, json({ "Error": "Bad formated request" })
        end

        #Creamos un nuevo post a partir del parseo anterior y carga todos.
        new_post = JSON.parse(requestContent)
        posts = load_posts

        #Solo permitimos un post por día.
        if find_post(posts, new_post["date"]) != nil
            halt 409, json({ "Error": "There is a post in that date" })
        end


        posts.push(new_post)
        save_posts(posts)
        new_post.to_json
    end

    get '/posts/:date' do |date|
        posts = load_posts
        post = find_post(posts, date)
        # Si no ha venido un error 404, si no lo devolvemos.
        if post == nil
            halt 404, json({})
        end

        post.to_json
    end

    delete '/posts/:date' do |date|
        posts = load_posts
        post = find_post(posts, date)

        if post == nil
            halt 404, json({})
        end

        posts.delete(post)
        save_posts(posts)

        status 204
    end

    put '/posts/:date' do |date|
        posts = load_posts
        post = find_post(posts, date)

        if post == nil
            halt 404, json({"Error": "No se encuentra el post seleccionado" })
        end

        posts.put(post)
        save_posts(posts)

        status 204
    end

end

# Aux functions

def load_posts
    file = File.read("posts.json")
    posts = JSON.parse(file)
    return posts
end

def save_posts(post_list)
    File.open("posts.json", "w") do |f|
        f.write(post_list.to_json)
    end
end

def valid_json?(string)
  begin
    !!JSON.parse(string)
  rescue JSON::ParserError
    false
  end
end

def find_post(posts, date)
    posts.each do |aPost|
        if aPost["date"] == date
            return aPost
            break
        end
    end
    return nil
end
